package office;

public abstract class Employee {
	
	private String name;
	private char group;
	private double basicSalary;
	
	public Employee(String name, char group) {
		this.name = name;
		this.group = group;
	}
	
	public abstract double getDailySalary();
	
	public boolean isIndianCitizen() {
		return true;
	}
	
	@Override
	public String toString() {
		return name + " group: " + group;
	}

}
