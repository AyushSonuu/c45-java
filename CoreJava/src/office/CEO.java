package office;

public class CEO extends Employee{
	
	public CEO(String name) {
		super(name, 'C');
	}
	
	@Override
	public double getDailySalary() {
		return Double.MAX_VALUE;
	}


}
