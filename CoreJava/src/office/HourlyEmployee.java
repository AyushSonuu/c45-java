package office;

public class HourlyEmployee extends Employee{
	
	private double hourlySalary;
	private int hoursWorked;
	
	public HourlyEmployee(String name, double hourlySalary, int hoursWorked) {
		super(name, 'A');
		this.hourlySalary = hourlySalary;
		this.hoursWorked = hoursWorked;
	}
	
	@Override
	public double getDailySalary() {
		return hoursWorked * hourlySalary;
	}

}
