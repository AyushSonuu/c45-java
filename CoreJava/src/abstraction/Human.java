package abstraction;

public class Human extends Animal{
	
	private long aadhaar;
	
	public Human(long aadhaar) {
		super("Homo sapiens");
		this.aadhaar = aadhaar;
	}
	
	@Override
	public String talk() {
		return "I am " + species + " and I say hello!";
	}

}
