package abstraction;

public abstract class Animal implements Talkative{
	
	protected String species;
	
	public Animal(String species) {
		this.species = species;
	}
	
	public String getSpecies() {
		return species;
	}

}
