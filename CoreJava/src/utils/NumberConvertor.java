package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NumberConvertor {
	
	private static Map<Character, Integer> arabics;
	private static Map<Integer, String> romans;
	
	static {
		arabics = new HashMap<Character, Integer>();
		arabics.put('I', 1);
		arabics.put('V', 5);
		arabics.put('X', 10);
		arabics.put('L', 50);
		arabics.put('C', 100);
		arabics.put('D', 500);
		arabics.put('M', 1000);
		//DATA IS CODE
	}
	
	static {
		romans = new HashMap<Integer, String>();
		romans.put(1, "I");
		romans.put(5, "V");
		romans.put(10, "X");
		romans.put(50, "L");
		romans.put(100, "C");
		romans.put(500, "D");
		romans.put(1000, "M");
		romans.put(4, "IV");
		romans.put(9, "IX");
		romans.put(40, "XL");
		romans.put(90, "XC");
		romans.put(400, "CD");
		romans.put(900, "CM");
	}
	
	public static String arabicToRoman(long arabic) {
		String roman = "";
		List<Integer> ns = new ArrayList<Integer> (romans.keySet());
		Collections.sort(ns);
		Collections.reverse(ns);
		
		for (Integer n : ns) {
			while (arabic >= n) {
				roman += romans.get(n);
				arabic -= n;
			}
		}
		return roman;
	}
	
	public static long romanToArabic(String roman) {
		roman = roman.toUpperCase().replace("IV", "IIII")
				  .replace("IX", "VIIII")
				  .replace("XL", "XXXX")
				  .replace("XC", "LXXXX")
				  .replace("CD", "CCCC")
				  .replace("CM", "DCCCC");
		long arabic = 0;
		for (Character c : roman.toCharArray())
			arabic += arabics.get(c);
		return arabic;
	}
	
	
	
	public static void main(String[] args) {
		System.out.println(romanToArabic("XXVII"));
		System.out.println(romanToArabic("IV"));
		System.out.println(romanToArabic("MMXXIII"));
		
		System.out.println(arabicToRoman(2023));
		System.out.println(arabicToRoman(4));
		
		Map<String, String> capitals = new HashMap<String, String> ();
		capitals.put("Andhra Pradesh", "Hyderabad");
		capitals.put("Tamil Nadu", "Chennai");
		capitals.put("Maharashtra", "Mumbai");
		capitals.put("India", "New Delhi");
		capitals.put("National Capital Region", "New Delhi");
		capitals.put("Andhra Pradesh", "Amaravati");
		
		System.out.println(capitals.get("Tamil Nadu"));
		System.out.println(capitals.get("Punjab"));
		System.out.println(capitals.get("India"));
		System.out.println(capitals.get("National Capital Region"));
		System.out.println(capitals.get("Andhra Pradesh"));
	}

}
