package utils;

import java.util.Random;

public class ShuffleUtil {
	public static void swap(Object[] array, int i, int j) {
		Object temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	public static void shuffle(int start, Object[] array) {
		Random r = new Random();
		for (int i = start; i < array.length; i++) {
			int randIndex = r.nextInt(start, array.length);
			swap(array, i, randIndex);
		}
	}
}
