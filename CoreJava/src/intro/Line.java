package intro;

public class Line {
	
	private static final double EPSILON = 0.00001;
	
	public double m;
	public double c;
	
	public Line(double m, double c) {
		this.m = m;
		this.c = c;
	}
	
	public Line(Point p1, Point p2) {
		if (p1.equals(p2))
			throw new ArithmeticException("can't form a line");
		m = (p2.getY() - p1.getY()) / (p1.getX() - p2.getX());
		c = p2.getY() - m * p2.getX();
	}
	
	public String toString() {
		return "y = " + m + " * x + " + c;
	}
	
	public boolean contains(Point p) {
		double diff = Math.abs(p.getY() - (m * p.getX() + c));
		return diff < EPSILON;
	}
	
	public Point intersection(Line other) {
		double slopediff = Math.abs(other.m - this.m);
		if (slopediff < EPSILON)
			throw new ArithmeticException("Equal slopes");
		double interX = (other.c - this.c) / (this.m - other.m);
		double interY = (this.c / this.m - other.c / other.m) /
							(1 / this.m - 1 / other.m);
		return new Point(interX, interY);
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Line))
			return false;
		Line otherLine = (Line) other;
		return this.m == otherLine.m && this.c == otherLine.c;
	}

}
