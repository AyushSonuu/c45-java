package intro;

public class Dog {
	
	private String name;
	private String breed;
	private String colour;
	private int age;
	
	public static final String SPECIES = "Canis Familaris";
	
	public static String bark() {
		return "woof!";
	}

	public Dog(String name, String breed, String colour, int age) {
		this.name = name;
		this.breed = breed;
		this.colour = colour;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBreed() {
		return breed;
	}

	public String getColour() {
		return colour;
	}

	public int getAge() {
		return age;
	}

	public void grow() {
		this.age += 1;
	}
	
	
	
	

}
