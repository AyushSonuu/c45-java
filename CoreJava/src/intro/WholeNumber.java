package intro;

public class WholeNumber {
	
	private int num;
	
	public WholeNumber() {
		num = 0;
	}
	
	public WholeNumber(int num) throws WholeNumberException {
		if (num < 0)
			throw new WholeNumberException("Whole numbers can't be negative");
		this.num = num;
	}
	
	public double divide(WholeNumber other) {
		if (other.num == 0)
			throw new ArithmeticException("/ by 0");
		return (double)num / other.num;
	}
	
	public String toString() {
		return String.valueOf(num);
	}

}
