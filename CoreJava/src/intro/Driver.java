package intro;

import java.util.Scanner;

public class Driver {
	
	public static void main(String[] args) {
//		Point onetwo = new Point(1, 2);
//		Point origin = new Point();
//		
//		System.out.println(onetwo);
//		System.out.println(origin);
//		System.out.println(onetwo.distanceFrom(origin));
//		
//		Circle c = new Circle(origin, 5);
//		System.out.println(c.area());
//		System.out.println(c.circumference());
//		System.out.println(c.contains(onetwo));
//		
//		System.out.println(origin.getX());
		
//		Line l1 = new Line(3, 0);
//		Line l2 = new Line(3, 0);
//		System.out.println(l1.contains(new Point(1.1, 3.3)));
//		System.out.println(l1.intersection(l2));
		
//		Fraction f1 = new Fraction(2, 6);
//		Fraction f2 = new Fraction(2, 3);
//		System.out.println(f1.add(f2));
		
		
//		System.out.println(Dog.SPECIES);
//		Dog d = new Dog("Jimmy", "Retriever", "Brown", 7);
//		d.grow();
//		System.out.println(d.getAge());
//		System.out.println(Dog.bark());
		
//		Date d = new Date(28, 2, 1999);
//		System.out.println(d.nextDay());
		
//		try {
//			WholeNumber n1 = new WholeNumber(-3);
//			WholeNumber n2 = new WholeNumber(25);
//			WholeNumber n3 = new WholeNumber();
//			System.out.println(n1);
//			System.out.println(n2.divide(n1));
//			System.out.println(n3);
//		} catch(WholeNumberException e) {
//			System.out.println("yippee");
//			e.printStackTrace();
//		} catch(ArithmeticException e) {
//			System.out.println("wahoo");
//			e.printStackTrace();
//		}
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("Enter a whole number");
			int num = sc.nextInt();
			try {
				WholeNumber n = new WholeNumber(num);
				System.out.println("The number you have entered is " + n);
				break;
			}
			catch (WholeNumberException e) {
				System.out.println("We only accept whole numbers");
			}
			
		}
		
	}

}
