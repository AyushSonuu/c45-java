package intro;

public class Point {
	
	private double x;
	private double y;
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public Point() {
		x = 0;
		y = 0;
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double distanceFrom(Point p) {
		return Math.sqrt(Math.pow(p.x - this.x, 2)
				+ Math.pow(p.y - this.y,  2));
	}
	
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	

}
