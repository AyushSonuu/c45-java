package cardgamecollections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cardgames.Card;

public class Hand {
	private List<Card> cards;
	
	public Hand() {
		cards = new ArrayList<Card>();
	}
	
	public int search(Card c) {
		return cards.indexOf(c);
	}
	
	public void add(Card c) {
		if (search(c) == -1)
			cards.add(c);
	}
	
	public void remove(Card c) {
		cards.remove(c);
	}
	
	public void remove(int index) {
		cards.remove(index);
	}
	
	public void sort() {
		Collections.sort(cards);
	}
	
}
