package drivers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import abstraction.Animal;
import abstraction.Cat;
import abstraction.Human;
import abstraction.Robot;
import abstraction.Talkative;
import cardgames.Card;
import cardgames.CardComparator;
import cardgames.CardReverseComparator;
import cardgames.Hand;
import cardgames.InvalidRankException;
import cardgames.InvalidSuitException;
import chess.King;
import chess.Piece;
import chess.Position;
import office.CEO;
import office.Employee;
import office.FullTimeEmployee;
import office.HourlyEmployee;

;

public class Driver {
	
	/*
	 * returns true for the input "abcde"
	 * returns false for the input "abcda"
	 */
	public static boolean everyCharacterIsUnique(String s) {
		Set<Character> charSet = new HashSet<Character> ();
		for (int i = 0; i < s.length(); i++) {
			charSet.add(s.charAt(i));
		}
		for (Character c : charSet)
			System.out.println(c.hashCode());
		return charSet.size() == s.length();
	}
	
	public static void main(String[] args) {
//		HourlyEmployee e1 = new HourlyEmployee("Aditya", 1000, 6);
//		FullTimeEmployee e2 = new FullTimeEmployee("Aruvi", 30000);
//		CEO e3 = new CEO("Santanu");
//		
//		System.out.println(e3.isIndianCitizen());
//		
//		Employee[] allEmployees = {e1, e2, e3};
//		
//		for (Employee emp : allEmployees) {
//			System.out.println(emp.getDailySalary());
//		}
//		System.out.println(e1 instanceof HourlyEmployee);
//		System.out.println(e1 instanceof Employee);
//		//System.out.println(e1 instanceof CEO);
		
//		Human h = new Human(123412341234l);
//		Cat c = new Cat();
//		Robot r = new Robot();
//		
//		Talkative[] things = {h, c, r};
//		
//		for (Talkative thing : things)
//			System.out.println(thing.talk());
//		
//		System.out.println(h.getSpecies());
//		System.out.println(h);
		
//		Piece r = new King(Piece.BLACK, new Position(1, 1));
//		System.out.println(r.canMove(new Position(3, 3)));
		
//		cardgamecollections.Hand h = new cardgamecollections.Hand();
//		h.remove(1);
//		try {
//		Card c1 = new Card(Card.SPADES, Card.ACE);
//		Card c2 = new Card(Card.SPADES, Card.ACE);
//		Card c3 = new Card(Card.SPADES, Card.TWO);
//		Hand h = new Hand();
//		h.add(c1);
//		h.add(c2);
//		h.add(c3);
//		
//		System.out.println(h);
//		
//		System.out.println(h.containsUniqueCards());
//		
//		} catch(InvalidSuitException | InvalidRankException e) {
//			e.printStackTrace();
//		}
		System.out.println(everyCharacterIsUnique("abcde"));
		System.out.println(everyCharacterIsUnique("abcda"));

	}
}
