package cardgames;

public class Card implements Comparable<Card>{
	
	private int rank;
	private int suit;
	
	public static final int SPADES = 1;
	public static final int HEARTS = 2;
	public static final int DIAMONDS = 3;
	public static final int CLUBS = 4;
	
	public static final String[] SUITS = {"", "SPADES", "HEARTS", "DIAMONDS", "CLUBS"};
	
	public static final int ACE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int TEN = 10;
	public static final int JACK = 11;
	public static final int QUEEN = 12;
	public static final int KING = 13;

	public static final String[] RANKS = {"", "ACE", "TWO",
			"THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT",
			"NINE", "TEN", "JACK", "QUEEN", "KING"};
	
	public Card(int suit, int rank) 
			throws InvalidSuitException, InvalidRankException {
		setSuit(suit);
		setRank(rank);
	}
	
	public void setSuit(int suit) throws InvalidSuitException {
		if (suit < SPADES || suit > CLUBS) {
			throw new InvalidSuitException("Invalid suit");	
		}
		this.suit = suit;
	}
	
	public void setRank(int rank) throws InvalidRankException{
		if (rank < ACE || rank > KING) {
			throw new InvalidRankException("Invalid rank");
		}
		this.rank = rank;
	}
	
	public int getRank() {
		return rank;
	}
	
	public int getSuit() {
		return suit;
	}
	
	@Override
	public String toString() {
		return RANKS[rank] + " of " + SUITS[suit];
	}
	
	@Override
	public boolean equals(Object other) {
		if (! (other instanceof Card))
			return false;
		Card otherCard = (Card) other;
		return this.rank == otherCard.rank && this.suit == otherCard.suit;
	}
	
	public boolean greaterThan(Card other) {
		if (this.suit > other.suit)
			return true;
		else
			if (this.suit == other.suit && this.rank > other.rank)
				return true;
			else
				return false;
	}

	@Override
	public int compareTo(Card arg0) {
		if (this.suit > arg0.suit) {
			return -1;
		}
		else if (this.suit == arg0.suit) {
			if (this.rank == arg0.rank)
				return 0;
			else if (this.rank > arg0.rank)
				return -1;
			else
				return 1;
		}
		else
			return 1;
	}
	
	@Override
	public int hashCode() {
		return rank + 10 * suit;//the number whose first digit is suit and second digit is rank
	}
}
