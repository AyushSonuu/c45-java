package cardgames;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import utils.ShuffleUtil;

public class Hand {
	
	private Card[] cards;
	
	public Hand() {
		cards = new Card[0];
	}
	
	public Hand(Card[] cards) {
		this.cards = cards;
	}
	
	public int score() {
		//do Chicago logic
		//https://en.wikipedia.org/wiki/Chicago_(poker_card_game)
		return 0;
	}
	
	public void sort() {
		boolean haveSwapped;
		do {
			haveSwapped = false;
			for (int i = 0; i < cards.length - 1; i++) {
				if (cards[i].greaterThan(cards[i + 1])) {
					ShuffleUtil.swap(cards, i, i + 1);
					haveSwapped = true;
				}
			}
		} while (haveSwapped);
	}
	
	public void add(Card c) {
		Card[] temp = new Card[cards.length + 1];
		int i = 0;
		for (Card x : cards)
			temp[i++] = x;
		temp[i] = c;
		cards = temp;
	}
	
	public boolean remove(Card c) {
		int index = search(c);
		if (index != -1) {
			remove(search(c));
			return true;
		}
		return false;
	}
	
	public int search(Card c) {
		for (int i = 0; i < cards.length; i++) {
			if (cards[i].equals(c))
				return i;
		}
		return -1;
	}
	
	public void remove(int index) {
		Card[] temp = new Card[cards.length - 1];
		int j = 0;
		for (int i = 0; i < cards.length; i++) {
			if (i == index)
				continue;
			temp[j++] = cards[i];
		}
		cards = temp;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(cards);
	}
	
	public boolean containsUniqueCards() {
		Set<Card> uniqueCardSet = new HashSet<Card>();
		for (Card c : cards)
			uniqueCardSet.add(c);
		return uniqueCardSet.size() == cards.length;
	}

}
