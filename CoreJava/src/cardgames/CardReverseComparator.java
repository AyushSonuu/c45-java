package cardgames;

import java.util.Comparator;

public class CardReverseComparator implements Comparator<Card>{

	@Override
	public int compare(Card arg0, Card arg1) {
		if (arg0.getSuit() > arg1.getSuit())
			return -1;
		if (arg0.getSuit() == arg1.getSuit()) {
			if (arg0.getRank() > arg1.getRank())
				return -1;
			if (arg0.getRank() == arg1.getRank())
				return 0;
			return 1;
		}
		return 1;
	}

}
