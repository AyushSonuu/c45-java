package cardgames;

import java.util.Arrays;
import java.util.Random;

import utils.ShuffleUtil;

public class Deck {
	
	private Card[] cards;
	private int topIndex;
	
	public Deck() {
		topIndex = 0;
		cards = new Card[52];
		int k = 0;
		for(int i = Card.SPADES; i <= Card.CLUBS; i++)
			for (int j = Card.ACE; j <= Card.KING; j++)
				try {
					cards[k++] = new Card(i, j);
				} catch (InvalidSuitException e) {
					//suppress
				} catch (InvalidRankException e) {
					//suppress
				}
	}
	
	public String toString() {
		String output = "Deck: [";
		for (Card c : cards) {
			output += c;
			output += ", \n";
		}
		output += "]";
		return output;
	}
	
	
	/*returns an index between topIndex and 52 if found
	-1 otherwise*/
	public int search(Card c) {
		for (int i = topIndex; i < cards.length; i++) {
			if (cards[i].equals(c))
				return i;
		}
		return -1;
	}
	
	public boolean returnCard(Card c) {
		if (search(c) == -1) {
			cards[--topIndex] = c;
			return true;
		}
		return false;
	}
	
	
	public void shuffle() {
		ShuffleUtil.shuffle(topIndex, cards);
	}
	
	public Card draw() {
		if (topIndex >= 52) {
			System.out.println("Empty Deck");
			return null;
		}
		return cards[topIndex++];
	}
	
	
}
