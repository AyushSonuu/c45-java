package cardgames;

public class InvalidSuitException extends Exception{
	
	public InvalidSuitException(String message) {
		super(message);
	}

}
