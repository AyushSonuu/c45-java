package wordle;

import java.util.Scanner;

public class Game {
	
	public static final int ALLOWED_GUESSES = 6;
	
	private Guess[] guesses;
	private int guessCount;
	
	private String correctAnswer;
	
	public Game() {
		this.guesses = new Guess[ALLOWED_GUESSES];
		guessCount = -1;
		correctAnswer = "BORAX";//replace with generation logic later
	}
	
	public void takeGuess() {
		while (true) {
			System.out.println("Guess the word!");
			Scanner sc = new Scanner(System.in);
			String guessedWord = sc.next();
			if (guessedWord.length() != 5) {
				System.out.println("Your guessed word should be 5 characters long!");
				continue;
			}
			guesses[++guessCount] = new Guess(sc.next());
			break;
		}
	}
	
	public void gameLoop() {
		while(true) {
			takeGuess();
			if (guesses[guessCount].evaluate(correctAnswer)) {
				System.out.println("Congratulations, you guessed right");
				break;
			}
			else
				System.out.println(guesses[guessCount].getFeedback());
			if (guessCount == ALLOWED_GUESSES - 1) {
				System.out.println("Sorry, you've run out of guesses");
				System.out.println("The correct answer was: " + correctAnswer);
				break;
			}
		}
	}
	
	public static void main(String[] args) {
		Game g = new Game();
		g.gameLoop();
	}

}
