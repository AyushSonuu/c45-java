package wordle;

public class Guess {
	
	public static final char CORRECT_POSITION = '*';
	public static final char WRONG_POSITION = '#';
	public static final char WRONG_CHAR = '_';
	
	private String guess;
	private String feedback;
	
	public boolean evaluate(String correctAnswer) {
		//need to write implementation
		return true;
	}
	
	public Guess(String guess) {
		this.guess = guess;
	}
	
	public String getFeedback() {
		return feedback;
	}

}
