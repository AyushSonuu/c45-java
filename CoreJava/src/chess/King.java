package chess;

public class King extends Piece{
	
	public King(boolean colour, Position position) {
		super(Piece.KING, colour, position);
	}
	
	@Override
	public boolean canMove(Position next) {
		return Position.getAbsRowDiff(this.position, next) <= 1 &&
				Position.getAbsColumnDiff(this.position, next) <= 1;
	}
	
}
