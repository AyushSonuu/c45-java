package student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Classroom {
	
	private List<Student> students;
	
	public Classroom() {
		students = new ArrayList<>();
	}
	
	public void add(Student s) {
		students.add(s);
	}
	
	public void remove(Student s) {
		students.remove(s);
	}
	
	public double mean() {
		double total = 0;
		for (Student s : students)
			total += s.getScore();
		return total / students.size();
	}
	
	public double median() {
		List<Student> temp = new ArrayList<Student>(students);
		Collections.sort(temp);
		int len = temp.size();
		if (len % 2 != 0)
			return temp.get(len / 2).getScore();
		else {
			int preMedian = temp.get(len / 2 - 1).getScore();
			int postMedian = temp.get(len / 2).getScore();
			return (preMedian + postMedian) / 2.0;
		}
	}
	
	public Student searchByRollNumber(int rollNumber) {
		//return the Student object with the rollNumber
		//return null otherwise
		return null;
	}

}
