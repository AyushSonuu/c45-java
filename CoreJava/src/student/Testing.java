package student;

public class Testing {
	
	public static void main(String[] args) {
		Student s1 = new Student(1, "A", 80);
		Student s2 = new Student(2, "B", 90);
		Student s3 = new Student(3, "C", 40);
		Classroom c = new Classroom();
		c.add(s1);
		c.add(s2);
		c.add(s3);
		
		System.out.println(c.mean());
		System.out.println(c.median());
	}

}
